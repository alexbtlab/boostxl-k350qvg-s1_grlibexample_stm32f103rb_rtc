/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

///* DriverLib Includes */
//#include "driverlib.h"

///* Standard Includes */
#include <stdint.h>

#include "stdlib.h"
#include <stdio.h>

///* GrLib Includes */
#include "grlib.h"
#include "button.h"
#include "imageButton.h"
#include "radioButton.h"
#include "checkbox.h"
#include "kitronix320x240x16_ssd2119_spi.h"
#include "images/images.h"
#include "touch_P401R.h"

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_rtc.h"

#include "RTC.h"
#include "grlib.h"
#include "HAL_MSP_EXP432P401R_KITRONIX320X240_SSD2119_SPI.h"
//#include "kitronix320x240x16_ssd2119_spi.h"
//#include "driverlib.h"
#include "LCD.h"


//*****************************************************************************
//
// HAL_MSP_EXP430F5529LP_KITRONIX320X240_SSD2119_SPI.h - Prototypes for the
//           KITRONIX320X240_SSD2119 LCD display driver.
//
//
//                 MSP430F5529                 BOOSTXL-K350QVG-S1
//                -----------------              ------------
//               |     P1.6/UCB0SIMO|---------> |LCD_SDI     |
//            /|\|                  |           |            |
//             | |      P1.5/UCB0CLK|---------> |LCD_SCL     |
//             --|RST               |           |            |
//               |              P5.0|---------> |LCD_SCS     |
//               |              P4.6|---------> |LCD_SDC     |
//               |              P3.5|---------> |LCD_RESET   |
//               |        P2.7/TA2.2|---------> |LCD_PWM     |
//               |                  |           |            |
//               |                  |            ------------
//                ------------------
//****************************************************************************

// MCLK FREQUENCY (in Hz)
#define HAL_LCD_MCLK_FREQUENCY      48000000


#define GRLIB_MSP432_MODE           1

#define X_COORDINATA 0
#define Y_COORDINATA 1
// Pins from MSP430 mode selection

// Definition of USCI base address to be used for SPI communication
#define LCD_EUSCI_MODULE              EUSCI_B0_BASE

// Definition of TIMER_A base address to be used for backlight control
#define LCD_TIMER_BASE_BKLT       TIMER_B0_BASE


//Touch screen context
touch_context g_sTouchContext;
Graphics_ImageButton primitiveButton;
Graphics_ImageButton imageButton;
Graphics_Button yesButton;
Graphics_Button noButton;

//// Graphic library context
Graphics_Context g_sContext;
Graphics_Context g_sContext2;
//Flag to know if a demo was run
bool g_ranDemo = false;






void Delay								(uint16_t msec);
void boardInit						(void);
void clockInit						(void);
void initializeDemoButtons(void);
void drawMainMenu					(void);
void runPrimitivesDemo		(void);
void runImagesDemo				(void);
void drawRestarDemo				(void);


void HAL_LCD_initLCD(void);
void HAL_LCD_writeCommand(uint8_t command);
void HAL_LCD_writeData(uint16_t data);
void HAL_LCD_delay(uint16_t msec);
void HAL_LCD_selectLCD(void);
void HAL_LCD_deselectLCD(void);
void HAL_LCD_initTimer(uint16_t captureCompareVal);
uint16_t HAL_LCD_getTimerCaptureCompare();
void HAL_LCD_setTimerDutyCycle(uint16_t dutyCycle);
void HAL_LCD_startTimerCounter(void);
void HAL_LCD_stopTimerCounter(void);
void ADC_Ini								(uint8_t coordinata)					;
void 			USART_Ini					(void) ;