#include "main.h"

//#include "RTC.h"

struct __FILE {int handle;};
FILE __stdout;

//#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)

	char str_lcd[10] = {0};
	char * pstr_lcd = str_lcd;

uint8_t RTC_second_flag = 0;
#include <stm32f10x.h>
#include <stm32f10x_rtc.h>
#include <stm32f10x_bkp.h>

#define USER_RTC_SECOND_INTERRUPT
#define RTC_CONSISTENCY_CHECK_REG		BKP_DR10
#define RTC_CONSYSTENCY_KEY_VALUE		0xA5A5
#define RTC_YEAR_REG					BKP_DR9		/* store year counter in backup reg in BCD format: 0000~9999 */
#define RTC_M_D_REG					BKP_DR8		/* store month & day in backup reg in BCD format: 01~12:01~31 */
#define SECONDS_PER_DAY				86400
#define RTC_FLAG_SECONDS				0
#define RTC_FLAG_DATE_UP				1
#define RTC_FLAG_TIME_UP				2
#define SET_BIT_POS(flag, bit)				((flag) |= (1<<(bit)))
#define CLEAR_BIT_POS(flag, bit)			((flag) &= ~(1<<(bit)))
#define CHECK_BIT_POS(flag, bit)			((flag) & (1<<(bit)))

typedef struct {
	uint16_t year;
	uint8_t	month;
	uint8_t day;
} date_s;

typedef struct {
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint8_t centiseconds;
} time_s;

uint8_t RTC_second_flag;	// RTC Flag bitfield

date_s sys_date;
time_s sys_time;
time_s my_time;
								//                0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24   25
uint8_t timestamp_buff[26];		// timestamp[]->[|2||0||1||4||/||0||2||/||1||3|| ||1||2||:||0||0||:||0||0||.||0||1||#||X||#||\00 |]

void RTC_Configuration(void);
void RTC_SetTime(time_s* new_time);
void RTC_SetDate(date_s* new_date);
uint8_t * RTC_GetTimestamp(uint8_t *timestamp_buff, uint8_t source_char);
void DateUpdate(uint32_t days_gone);
uint8_t CheckLeap(uint16_t Year);


void RTC_Configuration		(void){
	/* STM32 RTC initialization */
	volatile uint16_t tmp;
	uint32_t tmp2 = 0;
	/* Enable PWR and BKP clocks */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

	/* LSI clock stabilization time */
	for(tmp=0;tmp<5000;tmp++) { ; }

	if (BKP_ReadBackupRegister(RTC_CONSISTENCY_CHECK_REG) != RTC_CONSYSTENCY_KEY_VALUE) {
		/* Allow access to BKP Domain */
		PWR_BackupAccessCmd(ENABLE);
		/* Reset Backup Domain */
		BKP_DeInit();
		/* Enable LSE */
		RCC_LSEConfig(RCC_LSE_ON);
		/* Wait till LSE is ready */
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) { ; }
		/* Select LSE as RTC Clock Source */
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
		/* Enable RTC Clock */
		RCC_RTCCLKCmd(ENABLE);
		/* Wait for RTC registers synchronization */
		RTC_WaitForSynchro();
		/* Set RTC prescaler: set RTC period to 1sec */
		RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		/* Enable the RTC Second interrupt */
		RTC_ITConfig(RTC_IT_SEC, ENABLE);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		/* Set initial value */
		RTC_SetCounter( (uint32_t)((8*3600))); 					// here: 08:00:00
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		/* write date to backup registers */
		BKP_WriteBackupRegister(RTC_M_D_REG, ((0x01<<8)|0x01));	// January 1st
		BKP_WriteBackupRegister(RTC_YEAR_REG, 2014);			// Year 2014
		/* write RTC/backup consistency check value to register */
		BKP_WriteBackupRegister(RTC_CONSISTENCY_CHECK_REG, RTC_CONSYSTENCY_KEY_VALUE);
	} else {
		/* Wait for RTC registers synchronization */
		RTC_WaitForSynchro();
		/* Enable the RTC Second interrupt */
		RTC_ITConfig(RTC_IT_SEC, ENABLE);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
	}
	/* init system date & time structures */
	tmp = BKP_ReadBackupRegister(RTC_M_D_REG);
	sys_date.day = (uint8_t) tmp;
	sys_date.month = (uint8_t)(tmp>>8);
	sys_date.year = BKP_ReadBackupRegister(RTC_YEAR_REG);

	tmp2 = RTC_GetCounter();
	sys_time.centiseconds = 0;		// round up centiseconds value
	sys_time.seconds = tmp2 % 60;
	tmp2 /= 60;
	sys_time.minutes = tmp2 % 60;
	tmp2 /= 60;
	sys_time.hours = tmp2 % 24;
	/* system time & date are set */

#if 0
	/* Enable RTC clk/64 output at PC13-TAMPER pin */
	/* Disable the Tamper Pin */
	BKP_TamperPinCmd(DISABLE); /* To output RTCCLK/64 on Tamper pin, the tamper functionality must be disabled */
	/* Enable RTC Clock Output on Tamper Pin */
	BKP_RTCOutputConfig(BKP_RTCOutputSource_CalibClock);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_2MHz;

	GPIO_Init(GPIOC,&GPIO_InitStructure);	// set PC13 as RTC clk/64 out
#endif

	/* Close access to BKP Domain */
	PWR_BackupAccessCmd(DISABLE);
}

void RTC_IRQHandler				(void){
	NVIC_ClearPendingIRQ(RTC_IRQn);
	if (RTC_GetITStatus(RTC_IT_SEC) != RESET)
	{
		uint32_t tmp = 0;
		uint32_t tmp2 = 0;
		/* Clear the RTC Second interrupt */
		RTC_ClearITPendingBit(RTC_IT_SEC);

		SET_BIT_POS(RTC_second_flag, RTC_FLAG_SECONDS);

		tmp = RTC_GetCounter();
		tmp2 = tmp;
		if (CHECK_BIT_POS(RTC_second_flag, RTC_FLAG_TIME_UP)) {
			/* convert sys_time to RTC */
			tmp = sys_time.seconds;
			tmp += sys_time.minutes * 60;
			tmp += sys_time.hours * 3600;
			/* Allow access to BKP Domain */
			PWR_BackupAccessCmd(ENABLE);
			/* Wait until last write operation on RTC registers has finished */
			RTC_WaitForLastTask();
			/* Set initial value */
			RTC_SetCounter(tmp); 		// write new time to RTC
			/* Wait until last write operation on RTC registers has finished */
			RTC_WaitForLastTask();
			/* Lock access to BKP Domain */
			PWR_BackupAccessCmd(DISABLE);
			CLEAR_BIT_POS(RTC_second_flag, RTC_FLAG_TIME_UP);
		} else {
			/* fill up sys_time structure with actual time */
			sys_time.centiseconds = 0;		// round up centiseconds value
			sys_time.seconds = tmp % 60;
			tmp /= 60;
			sys_time.minutes = tmp % 60;
			tmp /= 60;
			sys_time.hours = tmp % 24;
		}

		if (CHECK_BIT_POS(RTC_second_flag, RTC_FLAG_DATE_UP)) {
			DateUpdate(0);
			CLEAR_BIT_POS(RTC_second_flag, RTC_FLAG_DATE_UP);
		}

		/* If counter is equal to 86399: one day was elapsed */
		if (tmp2 == 86399) {
			/* Allow access to BKP Domain */
			PWR_BackupAccessCmd(ENABLE);
			/* Wait until last write operation on RTC registers has finished */
			RTC_WaitForLastTask();
			/* Reset counter value */
			RTC_SetCounter(0x00000000);
			/* Wait until last write operation on RTC registers has finished */
			RTC_WaitForLastTask();
			/* Lock access to BKP Domain */
			PWR_BackupAccessCmd(DISABLE);
			/* set bit flag for next second date update */
			SET_BIT_POS(RTC_second_flag, RTC_FLAG_DATE_UP);
		} else if (tmp2 > 86399) {
			tmp = tmp2 / 86400;		// get days qtty running on battery
			/* Allow access to BKP Domain */
			PWR_BackupAccessCmd(ENABLE);
			/* Wait until last write operation on RTC registers has finished */
			RTC_WaitForLastTask();
			/* Reset counter value */
			RTC_SetCounter(tmp2 - (tmp * 86400) + 1);
			/* Wait until last write operation on RTC registers has finished */
			RTC_WaitForLastTask();
			/* Lock access to BKP Domain */
			PWR_BackupAccessCmd(DISABLE);
			/* Increment the date */
			DateUpdate(tmp);
		}
	}
}

void DateUpdate						(uint32_t days_gone){
	uint16_t tmp = BKP_ReadBackupRegister(RTC_M_D_REG);
	sys_date.day = (uint8_t) tmp;
	sys_date.month = (uint8_t)(tmp>>8);
	sys_date.year = BKP_ReadBackupRegister(RTC_YEAR_REG);

	 do {
		if(sys_date.month == 1 || sys_date.month == 3 || \
				sys_date.month == 5 || sys_date.month == 7 ||\
				sys_date.month == 8 || sys_date.month == 10 \
				|| sys_date.month == 12)
		{
			if(sys_date.day < 31)
			{
			  sys_date.day++;
			}
			/* Date structure member: sys_date.day = 31 */
			else
			{
				if(sys_date.month != 12)
				{
					sys_date.month++;
					sys_date.day = 1;
				}
				/* Date structure member: sys_date.day = 31 & sys_date.month =12 */
				else
				{
					sys_date.month = 1;
					sys_date.day = 1;
					sys_date.year++;
				}
			}
		}
		else if(sys_date.month == 4 || sys_date.month == 6 \
				|| sys_date.month == 9 ||sys_date.month == 11)
		{
			if(sys_date.day < 30)
			{
				sys_date.day++;
			}
			/* Date structure member: sys_date.day = 30 */
			else
			{
				sys_date.month++;
				sys_date.day = 1;
			}
		}
		else if(sys_date.month == 2)
		{
			if(sys_date.day < 28)
			{
				sys_date.day++;
			}
			else if(sys_date.day == 28)
			{
				/* Leap Year Correction */
				if(CheckLeap(sys_date.year))
				{
					sys_date.day++;
				}
				else
				{
					sys_date.month++;
					sys_date.day = 1;
				}
			}
			else if(sys_date.day == 29)
			{
				sys_date.month++;
				sys_date.day = 1;
			}
		}
		if (days_gone) days_gone--;
	} while (days_gone) ;

	tmp = (uint16_t) sys_date.day;
	tmp |= (sys_date.month<<8);
	/* Allow access to BKP Domain */
	PWR_BackupAccessCmd(ENABLE);
	/* store new date to backup registers */
	BKP_WriteBackupRegister(RTC_M_D_REG, tmp);
	BKP_WriteBackupRegister(RTC_YEAR_REG, sys_date.year);
	/* Lock access to BKP Domain */
	PWR_BackupAccessCmd(DISABLE);
}

uint8_t CheckLeap					(uint16_t Year){
	if((Year%400)==0)
	{
		return 1;
	}
	else if((Year%100)==0)
	{
		return 0;
	}
	else if((Year%4)==0)
	{
	    return 1;
	}
	else
	{
	    return 0;
	}
}

void RTC_SetTime					(time_s * new_time){
	sys_time.centiseconds = 0;
	sys_time.hours = new_time->hours;
	sys_time.minutes = new_time->minutes;
	sys_time.seconds = new_time->seconds;

	SET_BIT_POS(RTC_second_flag, RTC_FLAG_TIME_UP);
}

void RTC_SetDate					(date_s * new_date){
	sys_date.year = new_date->year;
	sys_date.month = new_date->month;
	sys_date.day = new_date->day;
	/* Allow access to BKP Domain */
	PWR_BackupAccessCmd(ENABLE);
	/* store new date to backup registers */
	BKP_WriteBackupRegister(RTC_M_D_REG, (((new_date->month)<<8)|(new_date->day)));	// January 1st
	BKP_WriteBackupRegister(RTC_YEAR_REG, new_date->year);			// Year 2014
	/* Lock access to BKP Domain */
	PWR_BackupAccessCmd(DISABLE);
}

uint8_t * RTC_GetTimestamp(uint8_t * timestamp_buff, uint8_t source_char){
	
	uint8_t tmp8 = 0;
	uint8_t tmp8_1 = 0;
	uint16_t tmp16 = 0;
	uint16_t tmp16_1 = 0;

	/* fill up constant symbols */
	timestamp_buff[4] = timestamp_buff[7] = '/';
	timestamp_buff[10] = ' ';
	timestamp_buff[13] = timestamp_buff[16] = ':';
	timestamp_buff[19] = '.';
	timestamp_buff[22] = timestamp_buff[24] = '|';
	///// test /////
	timestamp_buff[25] = 0x00;
//	timestamp_buff[25] = 0x0A;
	/* fill up timestamp request source char */
	timestamp_buff[23] = source_char;
	/* fill up system date & time values */
	/* YEAR */
	tmp16 = sys_date.year;
	tmp16_1 = tmp16 / 1000;
	timestamp_buff[0] = (uint8_t) (tmp16_1 + 0x30);
	tmp16 -= tmp16_1 * 1000;
	tmp16_1 = tmp16 / 100;
	timestamp_buff[1] = (uint8_t) (tmp16_1 + 0x30);
	tmp16 -= tmp16_1 * 100;
	tmp16_1 = tmp16 / 10;
	timestamp_buff[2] = (uint8_t) (tmp16_1 + 0x30);
	tmp16 -= tmp16_1 * 10;
	timestamp_buff[3] = (uint8_t) (tmp16 + 0x30);
	/* MONTH */
	tmp8 = sys_date.month;
	tmp8_1 = tmp8 / 10;
	timestamp_buff[5] = (tmp8_1 + 0x30);
	tmp8 -= tmp8_1 * 10;
	timestamp_buff[6] = (tmp8 + 0x30);
	/* DAY */
	tmp8 = sys_date.day;
	tmp8_1 = tmp8 / 10;
	timestamp_buff[8] = (tmp8_1 + 0x30);
	tmp8 -= tmp8_1 * 10;
	timestamp_buff[9] = (tmp8 + 0x30);
	/* HOURS */
	tmp8 = sys_time.hours;
	tmp8_1 = tmp8 / 10;
	timestamp_buff[11] = (tmp8_1 + 0x30);
	tmp8 -= tmp8_1 * 10;
	timestamp_buff[12] = (tmp8 + 0x30);
	/* MINUTES */
	tmp8 = sys_time.minutes;
	tmp8_1 = tmp8 / 10;
	timestamp_buff[14] = (tmp8_1 + 0x30);
	tmp8 -= tmp8_1 * 10;
	timestamp_buff[15] = (tmp8 + 0x30);
	/* SECONDS */
	tmp8 = sys_time.seconds;
	tmp8_1 = tmp8 / 10;
	timestamp_buff[17] = (tmp8_1 + 0x30);
	tmp8 -= tmp8_1 * 10;
	timestamp_buff[18] = (tmp8 + 0x30);
	/* CENTISECONDS */
	tmp8 = sys_time.centiseconds;
	tmp8_1 = tmp8 / 10;
	timestamp_buff[20] = (tmp8_1 + 0x30);
	tmp8 -= tmp8_1 * 10;
	timestamp_buff[21] = (tmp8 + 0x30);
	/* return pointer to timestamp string buffer */
	return &timestamp_buff[0];
}

/*--------------------------------------------------------------------------------------------------------------------------------------*/
int fputc(int ch, FILE *f){//PUTCHAR_PROTOTYPE														{		// ��� ������� �� UART ����� printf()

	USART_SendData(USART1, (uint8_t) ch);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == SET){}		//�������. ��������� 

	return ch;
}
void initializeDemoButtons	(void)					{
   
	// Initiliaze primitives Demo Button
    primitiveButton.xPosition=20;
    primitiveButton.yPosition=50;
    primitiveButton.borderWidth=5;
    primitiveButton.selected=false;
    primitiveButton.imageWidth=Primitives_Button4BPP_UNCOMP.xSize;
    primitiveButton.imageHeight=Primitives_Button4BPP_UNCOMP.ySize;
    primitiveButton.borderColor=GRAPHICS_COLOR_WHITE;
    primitiveButton.selectedColor=GRAPHICS_COLOR_RED;
    primitiveButton.image=&Primitives_Button4BPP_UNCOMP;


    // Initiliaze images Demo Button
    imageButton.xPosition=180;
    imageButton.yPosition=50;
    imageButton.borderWidth=5;
    imageButton.selected=false;
    imageButton.imageWidth=Primitives_Button4BPP_UNCOMP.xSize;
    imageButton.imageHeight=Primitives_Button4BPP_UNCOMP.ySize;
    imageButton.borderColor=GRAPHICS_COLOR_WHITE;
    imageButton.selectedColor=GRAPHICS_COLOR_RED;
    imageButton.image=&Images_Button4BPP_UNCOMP;



    yesButton.xMin = 80;
    yesButton.xMax = 150;
    yesButton.yMin = 80;
    yesButton.yMax = 120;
    yesButton.borderWidth = 1;
    yesButton.selected = false;
    yesButton.fillColor = GRAPHICS_COLOR_RED;
    yesButton.borderColor = GRAPHICS_COLOR_RED;
    yesButton.selectedColor = GRAPHICS_COLOR_BLACK;
    yesButton.textColor = GRAPHICS_COLOR_BLACK;
    yesButton.selectedTextColor = GRAPHICS_COLOR_RED;
    yesButton.textXPos = 100;
    yesButton.textYPos = 90;
    yesButton.text = "YES";
    yesButton.font = &g_sFontCm18;

    noButton.xMin = 180;
    noButton.xMax = 250;
    noButton.yMin = 80;
    noButton.yMax = 120;
    noButton.borderWidth = 1;
    noButton.selected = false;
    noButton.fillColor = GRAPHICS_COLOR_RED;
    noButton.borderColor = GRAPHICS_COLOR_RED;
    noButton.selectedColor = GRAPHICS_COLOR_BLACK;
    noButton.textColor = GRAPHICS_COLOR_BLACK;
    noButton.selectedTextColor = GRAPHICS_COLOR_RED;
    noButton.textXPos = 200;
    noButton.textYPos = 90;
    noButton.text = "NO";
    noButton.font = &g_sFontCm18;
}
void drawMainMenu						(void)					{


		
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_LIGHT_YELLOW);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    Graphics_clearDisplay(&g_sContext);
    
		Graphics_drawStringCentered(&g_sContext, "Test GrLib on STM32F100",  AUTO_STRING_LENGTH, 159, 15,  TRANSPARENT_TEXT);
	//	sprintf(pstr_lcd, "Data=%#x" , 0x5f);
	//	Graphics_drawStringCentered(&g_sContext, pstr_lcd,      						 AUTO_STRING_LENGTH, 159, 35,  TRANSPARENT_TEXT);
    // Draw TI banner at the bottom of screnn
    Graphics_drawImage(&g_sContext, &zxcv8BPP_UNCOMP,   0,                     Graphics_getDisplayHeight(&g_sContext) - zxcv8BPP_UNCOMP.ySize);
		Graphics_drawImage(&g_sContext, &Google8BPP_UNCOMP, zxcv8BPP_UNCOMP.xSize, Graphics_getDisplayHeight(&g_sContext) - Google8BPP_UNCOMP.ySize);
	//	Graphics_drawImage(&g_sContext, &start8BPP_UNCOMP, 0, 0);
	
	
////	// Draw Primitives image button
//	Graphics_drawImageButton(&g_sContext, &primitiveButton);

////    // Draw Images image button
//    Graphics_drawImageButton(&g_sContext, &imageButton);
}

void runPrimitivesDemo			(void)					{
    int16_t ulIdx;
    uint32_t color;

    Graphics_Rectangle myRectangle1 = { 10, 50, 155, 120};
    Graphics_Rectangle myRectangle2 = { 150, 100, 300, 200};
    Graphics_Rectangle myRectangle3 = { 0, 0, 319, 239};

    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawString(&g_sContext, "Draw Pixels & Lines", AUTO_STRING_LENGTH, 60, 5, TRANSPARENT_TEXT);
    Graphics_drawPixel(&g_sContext, 45, 45);
    Graphics_drawPixel(&g_sContext, 45, 50);
    Graphics_drawPixel(&g_sContext, 50, 50);
    Graphics_drawPixel(&g_sContext, 50, 45);
    Graphics_drawLine(&g_sContext, 60, 60, 200, 200);
    Graphics_drawLine(&g_sContext, 30, 200, 200, 60);
    Graphics_drawLine(&g_sContext, 0, Graphics_getDisplayHeight(&g_sContext) - 1,
            Graphics_getDisplayWidth(&g_sContext) - 1,
            Graphics_getDisplayHeight(&g_sContext) - 1);
    Delay(2000);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "Draw Rectangles", AUTO_STRING_LENGTH, 159, 15, TRANSPARENT_TEXT);
    Graphics_drawRectangle(&g_sContext, &myRectangle1);
    Graphics_fillRectangle(&g_sContext, &myRectangle2);
    // Text won't be visible on screen due to transparency (foreground colors match)
    Graphics_drawStringCentered(&g_sContext, "Normal Text", AUTO_STRING_LENGTH, 225, 120, TRANSPARENT_TEXT);
    // Text draws foreground and background for opacity
    Graphics_drawStringCentered(&g_sContext, "Opaque Text", AUTO_STRING_LENGTH, 225, 150, OPAQUE_TEXT);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);

    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    // Text draws with inverted foreground color to become visible
    Graphics_drawStringCentered(&g_sContext, "Invert Text", AUTO_STRING_LENGTH, 225, 180, TRANSPARENT_TEXT);
    Delay(2000);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    // Invert the foreground and background colors
    Graphics_fillRectangle(&g_sContext, &myRectangle3);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_drawStringCentered(&g_sContext, "Invert Colors", AUTO_STRING_LENGTH, 159, 15, TRANSPARENT_TEXT);
    Graphics_drawRectangle(&g_sContext, &myRectangle1);
    Graphics_fillRectangle(&g_sContext, &myRectangle2);
    // Text won't be visible on screen due to transparency
    Graphics_drawStringCentered(&g_sContext, "Normal Text", AUTO_STRING_LENGTH, 225, 120, TRANSPARENT_TEXT);
    // Text draws foreground and background for opacity
    Graphics_drawStringCentered(&g_sContext, "Opaque Text", AUTO_STRING_LENGTH, 225, 150, OPAQUE_TEXT);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    // Text draws with inverted color to become visible
    Graphics_drawStringCentered(&g_sContext, "Invert Text", AUTO_STRING_LENGTH, 225, 180, TRANSPARENT_TEXT);
    Delay(2000);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "Draw Circles", AUTO_STRING_LENGTH, 159, 15, TRANSPARENT_TEXT);
    Graphics_drawCircle(&g_sContext, 100, 100, 50);
    Graphics_fillCircle(&g_sContext, 200, 140, 70);
    Delay(2000);
    Graphics_clearDisplay(&g_sContext);
    // Add some more color
    Graphics_setForegroundColor(&g_sContext, ClrLawnGreen);
    Graphics_setBackgroundColor(&g_sContext, ClrBlack);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "Rainbow of Colored Lines", AUTO_STRING_LENGTH, 159, 15, TRANSPARENT_TEXT);
    // Draw a quarter rectangle sweep of lines from red to purple.
    for(ulIdx = 128; ulIdx >= 1; ulIdx--)
    {
        // Red Color
        * ((uint16_t*) (&color)+1)  = 255;
        // Blue and Green Colors
        * ((uint16_t*) (&color))  = ((((128 - ulIdx) * 255) >> 7) << ClrBlueShift);

        Graphics_setForegroundColor(&g_sContext, color);
        Graphics_drawLine(&g_sContext, 160, 200, 32, ulIdx + 72);
    }
    // Draw a quarter rectangle sweep of lines from purple to blue.
    for(ulIdx = 128; ulIdx >= 1; ulIdx--)
    {
        // Red Color
        * ((uint16_t*) (&color)+1)  = (ulIdx * 255) >> 7;
        // Blue and Green Colors
        * ((uint16_t*) (&color))  = 255 << ClrBlueShift;

        Graphics_setForegroundColor(&g_sContext, color);
        Graphics_drawLine(&g_sContext, 160, 200, 160 - ulIdx, 72);
    }
    // Clear Red Color
    * ((uint16_t*) (&color)+1)  = 0;
    // Draw a quarter rectangle sweep of lines from blue to teal.
    for(ulIdx = 128; ulIdx >= 1; ulIdx--)
    {
        // Blue and Green Colors
        * ((uint16_t*) (&color))  = ((((128 - ulIdx) * 255) >> 7) << ClrGreenShift) | (255 << ClrBlueShift);

        Graphics_setForegroundColor(&g_sContext, color);
        Graphics_drawLine(&g_sContext, 160, 200, 288 - ulIdx, 72);
    }
    // Draw a quarter rectangle sweep of lines from teal to green.
    for(ulIdx = 128; ulIdx >= 0; ulIdx--)
    {
        // Blue and Green Colors
        * ((uint16_t*) (&color))  = (255 << ClrGreenShift) | (((ulIdx * 255) >> 7) << ClrBlueShift);

        Graphics_setForegroundColor(&g_sContext, color);
        Graphics_drawLine(&g_sContext, 160, 200, 288, 200 - (ulIdx));
    }
    Delay(2000);
    g_ranDemo = true;

    drawRestarDemo();

}



void runImagesDemo					(void)					{
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "Draw Uncompressed Image", AUTO_STRING_LENGTH, 159, 200, TRANSPARENT_TEXT);
    Delay(2000);
    // Draw Image on the display
    Graphics_drawImage(&g_sContext, &lcd_color_320x2408BPP_UNCOMP, 0, 0);
    Delay(2000);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "Draw RLE4 compressed Image", AUTO_STRING_LENGTH, 159, 200, TRANSPARENT_TEXT);
    Delay(2000);
    Graphics_drawImage(&g_sContext, &TI_logo_150x1501BPP_COMP_RLE4, 85, 45);
    Delay(2000);

    g_ranDemo = true;

    drawRestarDemo();
}

void drawRestarDemo					(void)					{
    g_ranDemo = false;
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext, "Would you like to go back",
        AUTO_STRING_LENGTH,
        159,
        45,
        TRANSPARENT_TEXT);
    Graphics_drawStringCentered(&g_sContext, "to the main menu?",
        AUTO_STRING_LENGTH,
        159,
        65,
        TRANSPARENT_TEXT);

    // Draw Primitives image button
    Graphics_drawButton(&g_sContext, &yesButton);

    // Draw Images image button
    Graphics_drawButton(&g_sContext, &noButton);

    do{
        touch_updateCurrentTouch(&g_sTouchContext);
        if(Graphics_isButtonSelected(&noButton, g_sTouchContext.x,  g_sTouchContext.y))
        {
            Graphics_drawSelectedButton(&g_sContext, &noButton);
            g_ranDemo = true;
        }else{
            if(g_ranDemo)
            {
                Graphics_drawReleasedButton(&g_sContext, &noButton);
                g_ranDemo = false;
            }
        }
    }while(!Graphics_isButtonSelected(&yesButton, g_sTouchContext.x,  g_sTouchContext.y));

    Graphics_drawSelectedButton(&g_sContext, &yesButton);

    g_ranDemo = true;
    Delay(1000);

}


void boardInit							(void)					{
//    FPU_enableModule();
}

void clockInit							(void)					{
	
	
//	RCC_HSECmd(ENABLE);
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSE);
	RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_9);
  RCC_PLLCmd(ENABLE);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /* 2 flash wait states, VCORE = 1, running off DC-DC, 48 MHz */
//    FlashCtl_setWaitState( FLASH_BANK0, 2);
//    FlashCtl_setWaitState( FLASH_BANK1, 2);
//    PCM_setPowerState( PCM_AM_DCDC_VCORE1 );
//    CS_setDCOCenteredFrequency( CS_DCO_FREQUENCY_48 );
//    CS_setDCOFrequency(48000000);
//    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, 1);
//    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, 1);
//    CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, 1);

    return;
}


void Delay									(uint16_t msec)	{
    
		uint32_t i=0;
    uint32_t time=(msec/1000)*(SYSTEM_CLOCK_SPEED/15);

    for(i=0;i<time;i++);
}
void USART_Ini							(void) 					{		// ��� ������� printf()
	
//	uint8_t DataByte = 0x01;
//	static uint8_t ReciveByte[16];
//	uint32_t i;
	
  USART_InitTypeDef USART_InitTypeDef_BLE;
	GPIO_InitTypeDef  GPIO_InitTypeDef_USART;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	/******************** ������������ SPI ******************************************/
	GPIO_StructInit(&GPIO_InitTypeDef_USART);
		GPIO_InitTypeDef_USART.GPIO_Mode 	= GPIO_Mode_AF_PP;	
		GPIO_InitTypeDef_USART.GPIO_Pin 	= GPIO_Pin_9 | GPIO_Pin_10;
		GPIO_InitTypeDef_USART.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitTypeDef_USART);
	
	
	USART_InitTypeDef_BLE.USART_BaudRate = 9600;//115200;
	USART_InitTypeDef_BLE.USART_WordLength = USART_WordLength_8b;
	USART_InitTypeDef_BLE.USART_StopBits = USART_StopBits_1;
	USART_InitTypeDef_BLE.USART_Parity = USART_Parity_No;
	USART_InitTypeDef_BLE.USART_Mode = USART_Mode_Tx;
	USART_InitTypeDef_BLE.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	
	USART_Init(USART1, &USART_InitTypeDef_BLE);
	
//	USART_ITConfig(USART1, USART_IT_TXE | USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
	
}
void ADC_Ini								(uint8_t coordinata)					{
    
	ADC_InitTypeDef  ADC_InitTypeDef_ADC;
	GPIO_InitTypeDef  GPIO_InitTypeDef_ADC;
	
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_ADC1, ENABLE);
	//RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
	
	
		if(coordinata == X_COORDINATA){
		
	GPIO_StructInit(&GPIO_InitTypeDef_ADC);	
		GPIO_InitTypeDef_ADC.GPIO_Mode 	= GPIO_Mode_Out_PP;	
			GPIO_InitTypeDef_ADC.GPIO_Pin 	= GPIO_Pin_1 | GPIO_Pin_3;
			GPIO_InitTypeDef_ADC.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitTypeDef_ADC);	
		
	//GPIO_SetBits  (GPIOA, GPIO_Pin_1);		// ������� �� Y
	GPIO_ResetBits(GPIOA, GPIO_Pin_3);
		
		
		GPIO_StructInit(&GPIO_InitTypeDef_ADC);	
			GPIO_InitTypeDef_ADC.GPIO_Mode 	= GPIO_Mode_AF_PP;	
			GPIO_InitTypeDef_ADC.GPIO_Pin 	= GPIO_Pin_0;
			GPIO_InitTypeDef_ADC.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitTypeDef_ADC);
	}
	
	else{
	
		GPIO_StructInit(&GPIO_InitTypeDef_ADC);		
			GPIO_InitTypeDef_ADC.GPIO_Mode 	= GPIO_Mode_Out_PP;	
			GPIO_InitTypeDef_ADC.GPIO_Pin 	= GPIO_Pin_0 | GPIO_Pin_2;
			GPIO_InitTypeDef_ADC.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitTypeDef_ADC);	
		
		GPIO_SetBits  (GPIOA, GPIO_Pin_0);		// ������� �� X
		GPIO_ResetBits(GPIOA, GPIO_Pin_2);
		
		GPIO_StructInit(&GPIO_InitTypeDef_ADC);	
			GPIO_InitTypeDef_ADC.GPIO_Mode 	= GPIO_Mode_AF_PP;	
			GPIO_InitTypeDef_ADC.GPIO_Pin 	= GPIO_Pin_1;
			GPIO_InitTypeDef_ADC.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitTypeDef_ADC);
	}
	
	
	
	
	
	/* ADC Configuration */
  ADC_DeInit(ADC1);
  ADC_StructInit(&ADC_InitTypeDef_ADC);
  ADC_InitTypeDef_ADC.ADC_ContinuousConvMode = DISABLE;
	ADC_InitTypeDef_ADC.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitTypeDef_ADC.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitTypeDef_ADC.ADC_Mode = ADC_Mode_Independent;
	if(coordinata == X_COORDINATA)	ADC_InitTypeDef_ADC.ADC_NbrOfChannel = 0;
	if(coordinata == Y_COORDINATA)	ADC_InitTypeDef_ADC.ADC_NbrOfChannel = 1;
	ADC_InitTypeDef_ADC.ADC_ScanConvMode = DISABLE;

//  sADC.ADC_SynchronousMode      = ADC_SyncMode_Independent; 
	ADC_Init(ADC1, &ADC_InitTypeDef_ADC);
	

  /* ADC1 enable */
  ADC_Cmd (ADC1, ENABLE);
	
	


	
	
	
}

void USART1_IRQHandler			(void)					{

	

}/*--------------------------------------------------------------------------------------------------------------------------------------*/
int main(void)
{
	Graphics_Rectangle Clear_Rect = {105,28,250,46};
	
	int64_t i, j = 0;
	int16_t cnt = 0;
	int16_t value_ADC = 0;
	time_s tms = {0,0,0,0};
	
	uint8_t    buf_timestp[200] = {0};
	uint8_t * pbuf_timestp = buf_timestp;
	
//  int16_t ulIdx;
//    WDT_A_hold(__WDT_A_BASE__);

    /* Initialize the demo. */
//    boardInit();
    clockInit();
		
				USART_Ini();
//    initializeDemoButtons();

    /* Globally enable interrupts. */
//    __enable_interrupt();

    // LCD setup using Graphics Library API calls
	
		Kitronix320x240x16_SSD2119Init();
    
		Graphics_initContext(&g_sContext, &g_sKitronix320x240x16_SSD2119);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_AQUA);
    Graphics_setFont(&g_sContext, &g_sFontCmss20b);
    Graphics_clearDisplay(&g_sContext);
	
//////    touch_initInterface();
		
   drawMainMenu();
//	


//	
//				Graphics_drawStringCentered(&g_sContext, "Start prog",
//        AUTO_STRING_LENGTH,
//        160,
//        35,
//        TRANSPARENT_TEXT);	
//			 
//				HAL_LCD_selectLCD();
//				for(j=0;j<40;j++)	{Kitronix320x240x16_SSD2119_setCursorLtoR(20, 150+j);		for(i=0;i<50;i++) 	HAL_LCD_writeData(0xFF00);}
//				HAL_LCD_deselectLCD();

//				Graphics_drawStringCentered(&g_sContext, "Yes",  AUTO_STRING_LENGTH,  45,  169,   TRANSPARENT_TEXT);	

//				HAL_LCD_selectLCD();
//				for(j=0;j<40;j++)	{Kitronix320x240x16_SSD2119_setCursorLtoR(80, 150+j);		for(i=0;i<50;i++) 	HAL_LCD_writeData(0xFF00);}
//				HAL_LCD_deselectLCD();

//				Graphics_drawStringCentered(&g_sContext, "No",  AUTO_STRING_LENGTH,  105,  169,   TRANSPARENT_TEXT);	

//				HAL_LCD_selectLCD();
//				for(j=0;j<40;j++)	{Kitronix320x240x16_SSD2119_setCursorLtoR(140, 150+j);		for(i=0;i<50;i++) 	HAL_LCD_writeData(0xFF00);}
//				HAL_LCD_deselectLCD();

//				Graphics_drawStringCentered(&g_sContext, "OK",  AUTO_STRING_LENGTH,  165,  169,   TRANSPARENT_TEXT);	

//				HAL_LCD_selectLCD();
//				for(j=0;j<40;j++)	{Kitronix320x240x16_SSD2119_setCursorLtoR(200, 150+j);		for(i=0;i<50;i++) 	HAL_LCD_writeData(0xFF00);}
//				HAL_LCD_deselectLCD();

//				Graphics_drawStringCentered(&g_sContext, "Err",  AUTO_STRING_LENGTH,  225,  169,   TRANSPARENT_TEXT);	

	//			USART_SendData(USART1, (uint8_t) 0x33);	 	

	//printf("1<-Startt  \n");
	
	//Graphics_setForegroundColorTranslated(&g_sContext, GRAPHICS_COLOR_BLACK);
//		ADC_Ini(X_COORDINATA);	
  
		RTC_SetCounter(0x0000);
	
		
		while(1)
    {
			
			
			
	//	pbuf_timestp = RTC_GetTimestamp(uint8_t * timestamp_buff, uint8_t source_char){
			
			
		//	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
		//	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET){}	
		//	value_ADC =		ADC_GetConversionValue(ADC1);	
			
			cnt = RTC_GetCounter();
			my_time.centiseconds = 0;		// round up centiseconds value
			my_time.seconds = cnt % 60;			cnt /= 60;
			my_time.minutes = cnt % 60;			cnt /= 60;
			my_time.hours   = cnt % 24;			
							
			sprintf(pstr_lcd, "%#d:%#d:%#d" , my_time.hours, my_time.minutes, my_time.seconds);
					
					Graphics_setForegroundColorTranslated(&g_sContext, GRAPHICS_COLOR_WHITE);
					Graphics_drawStringCentered(&g_sContext, pstr_lcd,   10, 159, 35,  TRANSPARENT_TEXT);
						
						for(j = 0 ; j <100 ; j++){}
							
					Graphics_setForegroundColorTranslated(&g_sContext, GRAPHICS_COLOR_BROWN);
					Graphics_fillRectangle(&g_sContext, &Clear_Rect);
					
						
					
								
							
							
							
							
							
							
							
							
							
//        touch_updateCurrentTouch(&g_sTouchContext);

//        if( g_sTouchContext.touch)
//        {
//            if(Graphics_isImageButtonSelected(&primitiveButton, g_sTouchContext.x,  g_sTouchContext.y)){
//                Graphics_drawSelectedImageButton(&g_sContext,&primitiveButton);
//              runPrimitivesDemo();
//            }else if (Graphics_isImageButtonSelected(&imageButton, g_sTouchContext.x,  g_sTouchContext.y)){
//                Graphics_drawSelectedImageButton(&g_sContext,&imageButton);
//                runImagesDemo();
//            }

//            if(g_ranDemo == true)
//            {
//                g_ranDemo = false;
//                drawMainMenu();
//            }
//        }
    }

}
