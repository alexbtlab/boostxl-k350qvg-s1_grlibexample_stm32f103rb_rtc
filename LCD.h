



#include "stm32f10x_spi.h"

// SYSTEM_CLOCK_SPEED (in Hz) allows to properly closeout SPI communication
#define SYSTEM_CLOCK_SPEED      48000000

// Ports from MSP430 connected to LCD
#define LCD_SDI_PORT       GPIOA
#define LCD_SCL_PORT       GPIOA
#define LCD_SCS_PORT       GPIOC
#define LCD_SDC_PORT       GPIOC
#define LCD_RESET_PORT     GPIOC
#define LCD_PWM_PORT       GPIOC
// Pins from MSP430 connected to LCD
#define LCD_SDI_PIN                      GPIO_Pin_7
#define LCD_SCL_PIN                      GPIO_Pin_5

#define LCD_SCS_PIN                      GPIO_Pin_0
#define LCD_SDC_PIN                      GPIO_Pin_1
#define LCD_RESET_PIN                    GPIO_Pin_2

#define LCD_PWM_PIN                      GPIO_Pin_3




void HAL_LCD_initLCD(void);
void HAL_LCD_writeCommand(uint8_t command);
void HAL_LCD_writeData(uint16_t data);
void HAL_LCD_delay(uint16_t msec);
void HAL_LCD_selectLCD(void);
void HAL_LCD_deselectLCD(void);
void HAL_LCD_initTimer(uint16_t captureCompareVal);
void HAL_LCD_setTimerDutyCycle(uint16_t dutyCycle);
void HAL_LCD_startTimerCounter(void);
void HAL_LCD_stopTimerCounter(void);

