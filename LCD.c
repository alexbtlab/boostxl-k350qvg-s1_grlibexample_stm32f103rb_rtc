#include "LCD.h"


void HAL_LCD_initLCD(void){
	
	SPI_InitTypeDef  SPI_InitTypeDef_MY;
	GPIO_InitTypeDef GPIO_InitTypeDef_SPI;
	GPIO_InitTypeDef GPIO_InitTypeDef_LED;
	GPIO_InitTypeDef GPIO_InitTypeDef_LCD_PWM;
	GPIO_InitTypeDef GPIO_InitTypeDef_CTRL_LCD;
	
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

/******************** ������������ SPI ******************************************/
	GPIO_StructInit(&GPIO_InitTypeDef_SPI);
		GPIO_InitTypeDef_SPI.GPIO_Mode 	= GPIO_Mode_AF_PP;	
		GPIO_InitTypeDef_SPI.GPIO_Pin 	= GPIO_Pin_5 | GPIO_Pin_7;
		GPIO_InitTypeDef_SPI.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitTypeDef_SPI);

/******** ������������ SPI *************************************************/
	SPI_I2S_DeInit(SPI1);

	/* SSP2 MASTER configuration ------------------------------------------------*/
	SPI_StructInit(&SPI_InitTypeDef_MY);
		SPI_InitTypeDef_MY.SPI_BaudRatePrescaler 	= SPI_BaudRatePrescaler_2;
		SPI_InitTypeDef_MY.SPI_CPHA 							= SPI_CPHA_2Edge;
		SPI_InitTypeDef_MY.SPI_CPOL 							= SPI_CPOL_High; 
		SPI_InitTypeDef_MY.SPI_DataSize 					= SPI_DataSize_8b;
		SPI_InitTypeDef_MY.SPI_Direction 					= SPI_Direction_1Line_Tx;
		SPI_InitTypeDef_MY.SPI_FirstBit 					= SPI_FirstBit_MSB;
		SPI_InitTypeDef_MY.SPI_Mode 							= SPI_Mode_Master;
		SPI_InitTypeDef_MY.SPI_NSS 								= SPI_NSS_Soft;
	SPI_Init(SPI1, &SPI_InitTypeDef_MY);
	SPI_Cmd(SPI1, ENABLE);

/******************** ������������ ������� LED ********************************/		
	//GPIO_StructInit(&GPIO_InitTypeDef_LED);
		GPIO_InitTypeDef_LED.GPIO_Mode 	= GPIO_Mode_Out_PP;	
		GPIO_InitTypeDef_LED.GPIO_Pin 	= GPIO_Pin_8 | GPIO_Pin_9;
		GPIO_InitTypeDef_LED.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOC, &GPIO_InitTypeDef_LED);

/******************** ������������ ������� SCS, SDC, RESET ********************************/		
	GPIO_StructInit(&GPIO_InitTypeDef_CTRL_LCD);
		GPIO_InitTypeDef_CTRL_LCD.GPIO_Mode 	= GPIO_Mode_Out_PP;	
		GPIO_InitTypeDef_CTRL_LCD.GPIO_Pin 	= GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
		GPIO_InitTypeDef_CTRL_LCD.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitTypeDef_CTRL_LCD);	

/****** LCD Backlight high to enable *******************************************/
	GPIO_StructInit(&GPIO_InitTypeDef_LCD_PWM);
		GPIO_InitTypeDef_SPI.GPIO_Mode 	= GPIO_Mode_Out_OD;	
		GPIO_InitTypeDef_SPI.GPIO_Pin 	= GPIO_Pin_5 | GPIO_Pin_7;
		GPIO_InitTypeDef_SPI.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitTypeDef_LCD_PWM);

/**** LCD control pins  ***************************************************************************/
	GPIO_SetBits(LCD_SDC_PORT,   LCD_SDC_PIN);
	GPIO_SetBits(LCD_SCS_PORT,   LCD_SCS_PIN);
	GPIO_SetBits(LCD_PWM_PORT,   LCD_PWM_PIN);

	HAL_LCD_delay(300);

		GPIO_SetBits(LCD_RESET_PORT, LCD_RESET_PIN);

	HAL_LCD_delay(300);
}
void HAL_LCD_writeCommand(uint8_t command){

	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET){}		  // Wait for the SPI transmission to complete before setting the LCD_SDC signal.
	GPIO_ResetBits(LCD_SDC_PORT, LCD_SDC_PIN);												// Set the LCD_SDC signal low, indicating that following writes are commands.
	SPI_I2S_SendData(SPI1, command);			 														// Transmit the command.
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET){}		  // Wait for the SPI transmission to complete before setting the LCD_SDC signal.
	GPIO_SetBits(LCD_SDC_PORT,LCD_SDC_PIN);														//// Set the LCD_SDC signal high, indicating that following writes are data.
}
void HAL_LCD_writeData(uint16_t data){
	
    uint8_t ui8Data;

  ui8Data = (uint8_t)(data >> 8);																							// Calculate the high byte to transmit.  
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET){}							// Wait for the transmit buffer to become empty.
	SPI_I2S_SendData(SPI1, ui8Data);																						// Transmit the high byte.

  ui8Data = (uint8_t)(data & 0xff);																						// Calculate the low byte to transmit.
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET){}							// Wait for the transmit buffer to become empty.
	SPI_I2S_SendData(SPI1, ui8Data);			 																			// Transmit the high byte.
		
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET){}	
}
void HAL_LCD_selectLCD(){

		while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET){}		// Wait for the SPI transmission to complete before setting the LCD_SDC signal.
		GPIO_ResetBits(LCD_SCS_PORT, LCD_SCS_PIN);
}
void HAL_LCD_deselectLCD(){

	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET){}		// Wait for the SPI transmission to complete before setting the LCD_SDC signal.
  GPIO_SetBits(LCD_SCS_PORT, LCD_SCS_PIN);
}
void HAL_LCD_delay(uint16_t msec){
    
		uint32_t i=0;
    uint32_t time=(msec/1000)*(SYSTEM_CLOCK_SPEED/15);

    for(i=0;i<time;i++);
}
